# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from hathi.front import (
    _collect_note, _collect_person, _dig_object
)

from activipy import vocab
from flask import url_for
import json
import requests
import logging
import hathi


class Test_collect_note(object):
    def test_summary_present(self, client):
        """Test _collect_note where summary is present"""

        obj = vocab.Note("http://localhost:8080/0", summary="Summary",
                         content="Bad data")

        link, summary = _collect_note(obj)
        assert link == url_for("note.index", id="http://localhost:8080/0")
        assert summary == "Summary"

    def test_summary_missing(self, client):
        """Test _collect_note where summary is present"""

        obj = vocab.Note("http://localhost:8080/0", content="Content")

        link, summary = _collect_note(obj)
        assert link == url_for("note.index", id="http://localhost:8080/0")
        assert summary == "http://localhost:8080/0"

    def test_summary_blank(self, client):
        """Test _collect_note where summary is present"""

        obj = vocab.Note("http://localhost:8080/0", content="Content",
                         summary="")

        link, summary = _collect_note(obj)
        assert link == url_for("note.index", id="http://localhost:8080/0")
        assert summary == "http://localhost:8080/0"


class Test_collect_person(object):
    def test_name_present(self, client):
        """Test _collect_person when name present"""

        obj = vocab.Person("http://localhost:8080/testperson",
                           name="Test Person")

        link, name = _collect_person(obj)
        assert link == url_for("actor.index",
                               id="http://localhost:8080/testperson")
        assert name == "Test Person"

    def test_name_absent(self, client):
        """Test _collect_person when name present"""

        obj = vocab.Person("http://localhost:8080/testperson")

        link, name = _collect_person(obj)
        assert link == url_for("actor.index",
                               id="http://localhost:8080/testperson")
        assert name == "http://localhost:8080/testperson"


class Test_dig_object(object):
    def test_create_idonly(self, client, monkeypatch):
        """Test create item with ID on object member"""

        def mock_get_obj(url):
            obj = None
            if url == "http://localhost:8080/0":
                obj = vocab.Create(url,
                                   **{"object": "http://localhost:8080/1"})
            elif url == "http://localhost:8080/1":
                obj = vocab.Note(url, summary="Test Note",
                                 content="Test Note")

            return obj

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        test_obj = _dig_object("http://localhost:8080/0")
        assert test_obj[0] == url_for("note.index",
                                      id="http://localhost:8080/1")
        assert test_obj[1] == "Test Note"

    def test_embedded_create(self, client, monkeypatch):
        """Test create with embedded object"""

        def mock_get_obj(url):
            obj = None
            if url == "http://localhost:8080/0":
                obj = vocab.Create(url,
                                   **{"object": vocab.Note(
                                        "http://localhost:8080/1",
                                        summary="Test Note",
                                        content="Test Note")})
            return obj

        monkeypatch.setattr(hathi.utils, "getObject", mock_get_obj)
        test_obj = _dig_object("http://localhost:8080/0")
        assert test_obj[0] == url_for("note.index",
                                      id="http://localhost:8080/1")
        assert test_obj[1] == "Test Note"

# vim: set expandtab:ts=4:sw=4
