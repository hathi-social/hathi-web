# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Test protocol module"""

import logging
import json
import socket

import pytest

from flask import Flask, session

from hathi import protocol

class ProtocolTestBase():
    """Base class for protocol-related testing

    This class has the helper functions to monkeypatch socket connections that
    can be used to control what data that socket.recv() will return, and check
    what data was sent on socket.sendall().

    This is named so because it's a helper class for the test suites below."""

    # pylint: disable=unused-argument,no-self-use,attribute-defined-outside-init

    def _init_app(self):
        app = Flask(__name__)
        app.config.from_mapping({'SECRET_KEY' : 'DEVKEY'})
        return app

    def _mock_create_connection(self, address, timeout=None):
        """Mock socket.create_connection()
        Simply return an empty socket.socket object since there's no real
        network connection needed."""

        return socket.socket()

    def _mock_sendall(self, data):
        """Mock socket.send_all()

        Capture what was passed to the function and return a "success" value."""
        self._send_data = data
        return len(self._send_data)

    def _mock_recv(self, bufsize):
        """Mock socket.recv()

        Return the data that was set before the call to socket.recv() is
        expected to happen."""

        return self._recv_data

    def monkeypatch_create_connection(self, monkeypatch):
        """Monkeypatch the socket.create_connection() function"""

        monkeypatch.setattr(socket, "create_connection",
                            self._mock_create_connection)

    def monkeypatch_recv(self, monkeypatch):
        """Monkeypatch the socket.recv() function"""

        monkeypatch.setattr(socket.socket, "recv", self._mock_recv)

    def monkeypatch_sendall(self, monkeypatch):
        """Monkeypatch the socket.sendall() function"""

        monkeypatch.setattr(
            socket.socket, "sendall", self._mock_sendall
        )

    def set_monkeypatches(self, monkeypatch):
        """Monkeypatch the necessary socket functions"""

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)
        self.monkeypatch_sendall(monkeypatch)

    def set_recv_data(self, expect_data):
        """Send the data that _mock_recv will return to the caller"""

        self._recv_data = bytes(json.dumps(expect_data, separators=(',', ':')), 'utf-8')

class TestProtocolBasics(ProtocolTestBase):
    """Test protocol module common operations"""

    # pylint: disable=protected-access


    def test_request_roll(self, monkeypatch, caplog):
        """Test request is being rolled per request"""

        caplog.set_level(logging.DEBUG, logger="Protocol")

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_sendall(monkeypatch)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)
            obj._send_msg({})
            assert session['protocol_request'] == 2

            request_data = json.loads(self._send_data.decode('utf-8'))
            assert request_data['request'] == 1
            assert request_data['version'] == 1

            obj._send_msg({})
            request_data = json.loads(self._send_data.decode('utf-8'))
            assert request_data['request'] == 2
            assert session['protocol_request'] == 3

    def test_required_fields(self, monkeypatch):
        """Test that all required fields are present"""

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)

        def function_missing(self):
            expect_data = {
                'request': 0,
                'version': 1,
            }
            self.set_recv_data(expect_data)

            obj = protocol.Protocol('localhost', 9999)

            with pytest.raises(KeyError) as excinfo:
                obj._recv_msg()
            assert 'function' in str(excinfo.value)

        def request_missing(self):
            expect_data = {
                'function': 'not-implemented',
                'version': 1,
            }
            self.set_recv_data(expect_data)

            obj = protocol.Protocol('localhost', 9999)

            with pytest.raises(KeyError) as excinfo:
                obj._recv_msg()
            assert 'request' in str(excinfo.value)

        def version_missing(self):
            expect_data = {
                'function': 'not-implemented',
                'request': 0,
            }
            self.set_recv_data(expect_data)

            obj = protocol.Protocol('localhost', 9999)

            with pytest.raises(KeyError) as excinfo:
                obj._recv_msg()
            assert 'version' in str(excinfo.value)

        with self._init_app().test_request_context():
            function_missing(self)
            request_missing(self)
            version_missing(self)

    def test_recv_msg_good(self, monkeypatch):
        """Test good base message"""

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)

        expect_data = {}
        expect_data['request'] = 1
        expect_data['version'] = 1
        expect_data['function'] = 'not-implemented'

        self.set_recv_data(expect_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)
            obj._expect_request = 1
            obj._recv_msg()

            assert expect_data['version'] == 1
            assert expect_data['request'] == 1

    def test_recv_msg_bad_request(self, monkeypatch):
        """Test message with bad request value"""

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)

        expect_data = {}
        expect_data['request'] = 100
        expect_data['version'] = 1
        expect_data['function'] = 'not-implemented'
        self.set_recv_data(expect_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException):
                obj = protocol.Protocol('localhost', 9999)
                obj._expect_request = 1
                obj._recv_msg()

    def test_recv_msg_bad_version(self, monkeypatch):
        """Test message with bad version value"""

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)

        expect_data = {}
        expect_data['request'] = 0
        expect_data['version'] = 100
        expect_data['function'] = 'not-implemented'
        self.set_recv_data(expect_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException):
                obj = protocol.Protocol('localhost', 9999)
                obj._recv_msg()

    def test_recv_msg_error(self, monkeypatch, caplog):
        """Test message where server responds with error"""

        caplog.set_level(logging.DEBUG, logger="Protocol")

        self.monkeypatch_create_connection(monkeypatch)
        self.monkeypatch_recv(monkeypatch)

        expect_data = {
            'request': 1,
            'version': 1,
            'function': 'result',
            'error': 'Some error',
        }
        self.set_recv_data(expect_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException, match='Some error'):
                obj = protocol.Protocol('localhost', 9999)
                obj._expect_request = 1
                obj._recv_msg()


class TestNewActor(ProtocolTestBase):
    """Test new-actor function implementation"""

    def test_success(self, monkeypatch):
        """Test successful identity creation"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
            'session':'0123456789abcdef123456789abcdef'
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)
            obj.new_actor('user', 'password')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'new-actor'
            assert req_data['username'] == 'user'
            assert req_data['password'] == 'password'

    def test_error(self, monkeypatch):
        """Test error encountered during identity creation"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
            'error':'Test Error'
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException,
                               match=r'.*Test Error$'):
                obj = protocol.Protocol('localhost', 9999)
                obj.new_actor('user', 'password')

    def test_session_missing(self, monkeypatch):
        """Test session field and error are both not present in response"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException,
                               match='Unexpected response from server'):
                obj = protocol.Protocol('localhost', 9999)
                obj.new_actor('user', 'password')

class TestLogin(ProtocolTestBase):
    """Test login function implementation"""

    def test_success(self, monkeypatch):
        """Test successful identity creation"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
            'session':'0123456789abcdef123456789abcdef'
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)
            obj.login('user', 'password')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'login'
            assert req_data['username'] == 'user'
            assert req_data['password'] == 'password'

    def test_bad_creds(self, monkeypatch, caplog):
        """Test invalid credential"""

        caplog.set_level(logging.DEBUG, logger="Protocol")
        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
            'error':'authentication failed'
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)
            assert obj.login('user', 'password') is None

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'login'
            assert req_data['username'] == 'user'
            assert req_data['password'] == 'password'

    def test_session_missing(self, monkeypatch):
        """Test session field and error are both not present in response"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException,
                               match='Unexpected response from server'):
                obj = protocol.Protocol('localhost', 9999)
                obj.new_actor('user', 'password')

    def test_error(self, monkeypatch):
        """Test error encountered during login processing"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function':'result',
            'request':1,
            'version':1,
            'error':'Test Error'
        }
        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            with pytest.raises(protocol.ProtocolException,
                               match=r'.*Test Error$'):
                obj = protocol.Protocol('localhost', 9999)
                obj.login('user', 'password')

class TestCheckSession(ProtocolTestBase):
    """Test check-session function implementation"""

    def test_valid(self, monkeypatch):
        """ Test good session """

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1,
            'session': 'mysession'
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            assert obj.check_session('mysession', 'asdf')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'check-session'
            assert req_data and req_data['session'] == 'mysession'
            assert req_data and req_data['actor'] == 'asdf'

    def test_invalid(self, monkeypatch):
        """Test invalid session"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'error': 'invalid',
            'version': 1,
            'request': 1
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            assert not obj.check_session('mysession', 'asdf')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'check-session'
            assert req_data['session'] == 'mysession'
            assert req_data['actor'] == 'asdf'

    def test_mismatch_session(self, monkeypatch):
        """Test where session received from server doesn't match what was sent"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1,
            'session': 'randomsession'
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            assert not obj.check_session('mysession', 'asdf')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'check-session'
            assert req_data['session'] == 'mysession'
            assert req_data['actor'] == 'asdf'

    def test_internal_error(self, monkeypatch):
        """Test other error"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'error': 'some-error',
            'version': 1,
            'request': 1
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            with pytest.raises(protocol.ProtocolException,
                               match="some-error"):
                obj.check_session('mysession', 'asdf')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'check-session'
            assert req_data['session'] == 'mysession'
            assert req_data['actor'] == 'asdf'

    def test_session_field_missing(self, monkeypatch):
        """Test when session field is missing"""

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            with pytest.raises(protocol.ProtocolException,
                               match="'session' field missing from response"):
                obj.check_session('mysession', 'asdf')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'check-session'
            assert req_data['session'] == 'mysession'
            assert req_data['actor'] == 'asdf'

class TestCheckLogout(ProtocolTestBase):
    """Test logout function implementation"""

    def test_valid(self, monkeypatch):
        """ Test good session """

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1,
            'session': 'mysession'
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            # Simply check that exceptions don't go leaking
            obj.logout('mysession')

            req_data = json.loads(self._send_data.decode('utf-8'))
            assert req_data['function'] == 'logout'
            assert req_data and req_data['session'] == 'mysession'

    def test_invalid_session(self, monkeypatch):
        """ Test session is invalid """

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1,
            'error': 'session invalid'
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            # Simply check that exceptions don't go leaking
            obj.logout('mysession')

    def test_server_error(self, monkeypatch):
        """ Test other server error """

        self.set_monkeypatches(monkeypatch)

        response_data = {
            'function': 'result',
            'version': 1,
            'request': 1,
            'error': 'some_error'
        }

        self.set_recv_data(response_data)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            # Simply check that exceptions don't go leaking
            obj.logout('mysession')

    def test_random_exception(self, monkeypatch):
        """Test handling of other exception not ProtocolException"""

        def throw_exception(self, bufsize):
            raise RuntimeError()

        self.monkeypatch_create_connection(monkeypatch)
        monkeypatch.setattr(socket.socket, "recv", throw_exception)

        with self._init_app().test_request_context():
            obj = protocol.Protocol('localhost', 9999)

            #Simply check that exceptions don't go leaking
            obj.logout('mysession')


# vim: set expandtab:ts=4:sw=4
