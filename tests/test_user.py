# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# pylint: disable=unused-argument,too-few-public-methods,invalid-name,no-self-use

"""Test user module"""

from hathi import user, utils

class TestUserLogin():
    """Test user.login()"""


    def test_valid(self, monkeypatch):
        """Test user is valid"""

        session = '0123456789abcdef0123456789abcdef'
        class MockProtocol:
            """Mock Protocol object"""

            def login(self, uname, passwd):
                """ Mock Protocol.login()"""

                nonlocal session
                return session

        def _mock_get_protocol():
            return MockProtocol()

        monkeypatch.setattr(utils, "get_protocol", _mock_get_protocol)

        u = user.User()
        assert u.login('user1', 'pass1')
        assert u._session == session # pylint: disable=protected-access

    def test_invalid(self, monkeypatch):
        """Test user is invalid"""

        class MockProtocol:
            """Mock Protocol object"""

            def login(self, uname, passwd):
                """ Mock Protocol.login()"""
                return None

        def _mock_get_protocol():
            return MockProtocol()

        monkeypatch.setattr(utils, "get_protocol", _mock_get_protocol)

        u = user.User()
        assert not u.login('user1', 'pass1')

class TestUserGetUser():
    """Test user.get_user()"""

    def test_valid(self, monkeypatch):
        """Test session is valid"""

        session = '0123456789abcdef0123456789abcdef'
        class MockProtocol:
            """Mock Protocol object"""

            def check_session(self, sess_id, actor):
                """Mock Protocol.check_session"""

                return True

        def _mock_get_protocol():
            return MockProtocol()

        monkeypatch.setattr(utils, "get_protocol", _mock_get_protocol)

        u = user.User()
        assert u.get_user(session, 'actor1') == u
        assert u._session == session # pylint: disable=protected-access

    def test_invalid(self, monkeypatch):
        """Test session is invalid"""

        class MockProtocol:
            """Mock Protocol object"""

            def check_session(self, uname, passwd):
                """Mock Protocol.check_session"""

                return False

        def _mock_get_protocol():
            return MockProtocol()

        monkeypatch.setattr(utils, "get_protocol", _mock_get_protocol)

        u = user.User()
        assert u.get_user('aaa', 'actor1') is None

    def test_exception(self, monkeypatch):
        """Test proper exception handling"""

        class MockProtocol:
            """Mock Protocol object"""

            def check_session(self, uname, passwd):
                """Mock Protocol.check_session"""

                from hathi import protocol
                raise protocol.ProtocolException('')

        def _mock_get_protocol():
            return MockProtocol()

        monkeypatch.setattr(utils, "get_protocol", _mock_get_protocol)

        u = user.User()
        assert u.get_user('aaa', 'actor1') is None


# vim: set expandtab:ts=4:sw=4
