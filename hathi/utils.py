# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import requests

from flask import g, current_app

from activipy import core
from .protocol import Protocol

def get_protocol():
    logger = logging.getLogger("Utils")

    if 'protocol' not in g:
        logger.debug("Creating new protocol instance")
        g.protocol = Protocol(current_app.config['HATHI_SERVER'], current_app.config['HATHI_PORT'])
    else:
        logger.debug("Using existing instance")

    return g.protocol


def getObject(url):
    logger = logging.getLogger("Utils")

    logger.debug("URL to get: %s", url)
    headers = {
        "Accept": "application/ld+json; "
        "profile=\"https://www.w3.org/ns/activitystreams\""
    }
    r = requests.get(url, headers=headers)
    if r.status_code == 404:
        logger.info("URL %s not found", url)
        return None
    if r.status_code != 200:
        cause = None
        try:
            data = r.json()
            cause = data["error"]
        except (JSONDecodeError, KeyError):
            cause = r.text

        errMsg = ("HTTP request for note \"%s\" returned HTTP code %d. "
                  "Cause: %s" % (url, r.status, cause))
        logger.error(errMsg)
        raise RuntimeError(errMsg)

    return core.ASObj(r.json())


def postObject(url, obj):
    logger = logging.getLogger("Utils")

    headers = {
        "Content-Type": "application/ld+json; "
        "profile=\"https://www.w3.org/ns/activitystreams\""
    }
    r = requests.post(url, json=obj.json(), headers=headers)
    if r.status_code != 201:
        logger.info("POST status code is %d" % (r.status_code))
        try:
            err = r.json()
            if "error" in err:
                errmsg = err["error"]
        except ValueError:
            errmsg = err.text
        raise RuntimeError("Failed to post object. Cause: %s" % (
                            errmsg if errmsg else "Unknown"))
    else:
        logger.debug("Object posted")

        try:
            retVal = r.headers["Location"]
            logger.debug("Create object location: %s", retVal)
        except KeyError:
            logger.exception("Server did not send \"Loction\" header. "
                             "Raw header: %s", r.headers)
            raise

    return retVal
# vim: set expandtab:ts=4:sw=4
