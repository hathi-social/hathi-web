# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from activipy import core, vocab

from flask import (
    Blueprint, flash, request, render_template, make_response, redirect,
    url_for
)

from flask_login import login_required, current_user

from . import utils, actor

bp = Blueprint("note", __name__, url_prefix="/note")


def collect_note(obj):
    """ Collect the Note with ID url

    Retrieve the Note object with ID url and return a tuple of the Note content
    """
    logger = logging.getLogger("Note")
    summary = None
    content = None

    obj = core.ASObj(obj)
    if "Note" not in obj.types:
        raise RuntimeError("Server returned invalid type")
    else:
        logger.debug("Object type valid")

    try:
        tmp = obj["summary"]
        if isinstance(tmp, str):
            summary = tmp
        else:
            logger.debug("Summary is an object. Available languages: %s",
                         tmp.keys())
            try:
                summary = tmp["en"]
            except KeyError:
                logger.debug("Summary doesn't have a en language string. "
                             "Using %s for key", (list(tmp.keys())[0]))
                summary = tmp[list(tmp.keys())[0]]

        logger.debug("Note summary: %s", tmp)
    except KeyError:
        logger.info("\"summary\" member not present.")

    try:
        tmp = obj["content"]
        if isinstance(tmp, str):
            content = tmp
        else:
            logger.debug("Content is an object")
            try:
                content = tmp["en"]
            except KeyError:
                logger.debug("Content doesn't have an en language string")
                content = tmp[list(tmp.keys())[0]]
        logger.debug("Note content: %s", content)
    except KeyError:
        logger.info("\"content\" member not present.")

    return (summary, content)


@bp.route("/", methods=["GET"])
@login_required
def index():
    """ Handle request for Note """
    logger = logging.getLogger("Note")

    try:
        uid = request.args["id"]
        logger.info("Get note \"%s\"", uid)
        logger.debug("Value of \"id\" query param: %s", uid)

        obj = utils.getObject(uid)
        if obj is None:
            flash("Note \"%s\" not available" % (uid))
            retVal = make_response(render_template("note/index.html"), 404)
        else:
            logger.debug("\"%s\" retrieved", uid)
            summary, content = collect_note(obj)

            reply_to = None
            reply_summary = None
            attributed_to = None

            try:
                reply_to = obj["inReplyTo"]
                reply_summary = collect_note(utils.getObject(reply_to))[:1][0]
            except KeyError:
                logger.debug("Note doesn't have an inReplyTo")

            try:
                actor_id = obj["attributedTo"]
                attributed_to = actor.get_name(utils.getObject(actor_id))
            except KeyError:
                logger.debug("Note doesn't have an attributedTo")
            except RuntimeError:
                logger.warning("Failed to retrieve actor %s", actor_id)

            logger.debug(
                "Reply to URL: %s. Reply to summary: \"%s\". " +
                "Attributed To: %s",
                reply_to, reply_summary, attributed_to
            )
            retVal = render_template("note/index.html", summary=summary,
                                     content=content,
                                     uid=uid, reply_to=reply_to,
                                     reply_summary=reply_summary,
                                     attributed_to=attributed_to)
    except KeyError as e:
        if "id" in e.args:
            flash("Note ID to view not provided", category="error")
            retVal = make_response(render_template("base.html"), 406)
        else:
            logger.exception("KeyError caught with unexpected key %s",
                             e.args)
            raise
    except Exception:
        logger.exception("Exception encountered rendering Note")
        flash("An error was encountered displaying a Note", category="error")
        retVal = make_response(render_template("base.html"), 500)

    logger.debug("Note object retrieved. Render the note")
    return retVal


def _build_note_obj(content, summary, to, cc, bcc, reply_to):
    """ Create the Note ActivityPub object """

    logger = logging.getLogger("Note")
    fields = {}

    if content:
        fields["content"] = content

    if summary:
        fields["summary"] = summary

    if to:
        fields["to"] = to

    if cc:
        fields["cc"] = cc

    if bcc:
        fields["bcc"] = bcc

    if reply_to:
        fields["inReplyTo"] = reply_to

    logger.debug("Fields: %s", fields)
    retVal = vocab.Note(None, **fields)

    logger.debug("Object to return: %s", retVal)
    return retVal


def _get_new_note_id(create_id):
    """ Get the newly created note's ID """

    logger = logging.getLogger("Note")

    obj = None
    try:
        obj = utils.getObject(create_id)
        if "Create" not in obj.types:
            msg = "%s is not a \"Create\" object" % create_id
            logger.error(msg)
            raise RuntimeError(msg)
    except RuntimeError:
        logger.exception("Error retrieving %s from server", create_id)
        raise

    logger.debug("Create object retrieved")
    note_obj = obj["object"]
    if not isinstance(note_obj, core.ASObj):
        raise AssertionError("%s object not an ActivityPub object." % (note_obj))
    else:
        if "Note" not in note_obj.types:
            raise AssertionError("Create object's payload not a Note")
        else:
            try:
                note_id = note_obj["@id"]
            except KeyError:
                logger.error("Note object missing ID attribute")
                raise

    logger.debug("Note UID to return: %s", note_id)
    return note_id


def _collect_recepient(params, field):
    logger = logging.getLogger("Note")
    retVal = []
    for v in params.getlist(field):
        if v != "":
            logger.debug("Adding %s to \"%s\"", v, field)
            retVal.append(v)

    if not len(retVal):
        logger.error("All values of field \"%s\" is empty", field)
        raise KeyError(field)

    return retVal


def _process_new_note(params, outbox):
    """ Process new note submitted """

    logger = logging.getLogger("Note")
    retVal = None

    # Only requiring content fieldA
    m_keys = []
    try:
        content = params.get("content")
        if not content or not len(content):
            raise KeyError("content")
    except KeyError:
        logger.info("\"content\" field is missing")
        m_keys.append("content")

    field = "to"
    to = None
    try:
        to = _collect_recepient(params, field)
    except KeyError:
        logger.info("\"%s\" field is missing", field)
        m_keys.append(field)

    field = "cc"
    cc = None
    try:
        cc = _collect_recepient(params, field)
    except KeyError:
        logger.info("\"%s\" field is missing", field)
        m_keys.append(field)

    field = "bcc"
    bcc = None
    try:
        bcc = _collect_recepient(params, field)
    except KeyError:
        logger.info("\"%s\" field is missing", field)
        m_keys.append(field)

    field = "reply_to"
    reply_to = None
    try:
        reply_to = params.get("reply_to")
    except KeyError:
        logger.debug("Not a reply note")

    if content and (to or cc or bcc):
        logger.debug("All required fields present")
        content = _build_note_obj(
            content,
            params["summary"] if "summary" in params else None,
            to, cc, bcc, reply_to
        )
        logger.debug("Value of content: %s", content.json())

        logger.debug("Outbox URL: %s", outbox)
        create_uid = None
        try:
            create_uid = utils.postObject(outbox, content)
            logger.debug("Note created. UID: %s", retVal)
            retVal = _get_new_note_id(create_uid)
        except (KeyError, RuntimeError, AssertionError) as e:
            logger.exception("Failed to POST new note to %s", outbox)
            raise RuntimeError(e)
    else:
        logger.info("Some required field missing. Reject request")
        if (to or cc or bcc):
            m_keys = [i for i in m_keys if i not in ("to", "cc", "bcc")]
        logger.debug("Value of m_keys for raise: %s", m_keys)
        raise KeyError(m_keys)

    logger.debug("New note ID: %s", retVal)
    return retVal


def _build_note_form(valid_fail=None, to_list=None, cc_list=None,
                     bcc_list=None, content=None, summary=None,
                     reply_to=None):
    """ Render the note view
    """
    return render_template("note/form.html", valid_fail=valid_fail,
                           to_list=to_list, cc_list=cc_list,
                           bcc_list=bcc_list, content=content,
                           summary=summary, reply_to=reply_to)


@bp.route("/new", methods=["GET"])
@login_required
def new_note():
    """ Handle HTTP POST on "/notes/new" """
    return _build_note_form()


@bp.route("/save", methods=["POST"])
@login_required
def save_note():
    logger = logging.getLogger("Note")
    retVal = None
    try:
        logger.info("Process new note")
        outbox = current_user.get_person_details()["outbox"]
        uid = _process_new_note(request.form, outbox)
        logger.debug("Note UID: %s", uid)
        flash("Note submitted successfully")
        retVal = make_response(redirect(url_for(".index", id=uid)))
    except (KeyError, RuntimeError) as e:
        logger.info("Fields missing from note submission %s", e.args[0])
        flash("An error occurred processing the new note")

        try:
            to_list = _collect_recepient(request.form, "to")
        except KeyError:
            to_list = None

        try:
            cc_list = _collect_recepient(request.form, "cc")
        except KeyError:
            cc_list = None

        try:
            bcc_list = _collect_recepient(request.form, "bcc")
        except KeyError:
            bcc_list = None

        retVal = _build_note_form(
            valid_fail=e.args[0],
            summary=request.form.get("summary"),
            content=request.form.get("content"),
            to_list=to_list,
            cc_list=cc_list,
            bcc_list=bcc_list,
            reply_to=request.form.get("reply_to")
        )

    return retVal


@bp.route("/reply", methods=["GET"])
@login_required
def reply_note():
    """ Render note reply form
    """

    logger = logging.getLogger("Note")
    retVal = None
    try:
        logger.info("Render reply to form")
        reply_to = request.args["replyto"]
        obj = collect_note(utils.getObject(reply_to))
        summary = obj[0]
        retVal = _build_note_form(reply_to=reply_to,
                                  summary="Re: %s" % (summary))
    except KeyError:
        logger.exception("Failed to render form for replying")
        flash("There's an issue attempting to reply to the note")
        retVal = retVal = make_response(redirect(url_for("front.index")))

    return retVal


# vim: set expandtab:ts=4:sw=4
