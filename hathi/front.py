# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging

from activipy import core, vocab

from .user import User
from . import utils, note, actor, protocol

from flask import (
    Blueprint, flash, redirect, render_template, request, url_for,
    make_response, current_app, session
)

from flask_login import current_user, login_user, logout_user, login_required

from werkzeug.urls import url_parse

bp = Blueprint("front", __name__, url_prefix="/")


@bp.route("/", methods=["GET"])
def index():
    logger = logging.getLogger("Front")

    if current_user.is_authenticated:
        logger.info("User logged in, redirect to actor view")
        retVal = redirect(url_for(".inbox"))
    else:
        logger.info("Request user login")
        next_page = request.args.get("next")
        logger.debug("Next destination: %s", next_page)
        logger.debug("Destination netloc: %s", (
            url_parse(next_page).netloc if next_page else "NONE")
        )
        if not next_page or url_parse(next_page).netloc != "":
            logger.info("Value of next query param, \"%s\", not acceptable "
                        "destination", next_page)
            next_page = None
        retVal = render_template("base.html", next_page=next_page)

    return retVal


@bp.route("/login", methods=["GET", "POST"])
def login():
    # pylint: disable=too-many-nested-blocks,too-many-branches

    """Handle login page"""

    logger = logging.getLogger("Front")

    if current_user.is_authenticated:
        ret_val = redirect(
            url_for("actor.index", **{"id": current_user.get_id()})
        )
    else:
        # pylint: disable=too-many-nested-blocks
        uname = request.form.get("uname")
        password = request.form.get("pass")

        if (uname is None
                or password is None
                or uname == ""
                or password == ""):
            logger.info("Username, password, or actor not provided")
            flash("Invalid username, password, or actor")
            ret_val = redirect(url_for(".index"))
        else:
            logger.debug("Username: \"%s\"", uname)
            logger.debug("Password: \"%s\"", ("*" * len(password)))

            try:
                user_obj = User()
                if user_obj.login(uname, password):
                    logger.info("User authenticated")
                    if login_user(user_obj):
                        next_page = request.form.get("next")
                        logger.debug("Next destination: %s", next_page)
                        if next_page and url_parse(next_page).netloc != "":
                            logger.info("Value of next query param, \"%s\","
                                        "not acceptable destination", next_page)
                            ret_val = redirect(url_for("actor.inbox"))
                        elif not next_page:
                            logger.info("Next query param not provided")
                            ret_val = redirect(url_for("front.inbox"))
                        else:
                            logger.info("Redirecting user back to %s", next_page)
                            ret_val = redirect(next_page)

                        logger.debug("Save the user-entered actor in session")
                    else:
                        logger.error("User failed to be logged-in after "
                                     "verification")
                else:
                    logger.info("Invalid credential provided")
                    flash("Invalid username or password")
                    ret_val = redirect(url_for(".index"))
            except (protocol.ProtocolException, IOError):
                logger.error("Error encountered during login")
                flash(
                    "Error encountered while validating credential. Please try again",
                    "error")
                ret_val = redirect(url_for(".index"))
            except BaseException:
                logger.exception("Exception encountered during login")

    return ret_val


@bp.route("/logout", methods=["GET"])
@login_required
def logout():
    logger = logging.getLogger("Front")

    if current_user.is_authenticated:
        sess_id = current_user.get_id()
        logger.info("Logging out user %s", sess_id)
        try:
            utils.get_protocol().logout(sess_id)
        except protocol.ProtocolException:
            logger.warning("Protocol exception encountered during logout")
        except BaseException:
            logger.exception("Communication exception encountered during logout")
    else:
        logger.debug("User not logged-in")

    logout_user()
    return redirect(url_for(".index"))


@bp.route("/register", methods=["GET", "POST"])
def register():
    logger = logging.getLogger("Front")
    template_name = "register.html"
    if request.method == "GET":
        logger.info("Render register form")
        retVal = render_template(template_name)
    elif request.method == "POST":
        logger.info("Process new user registration")
        fname = request.form.get("fname")
        uname = request.form.get("uname")
        passwd = request.form.get("passwd")

        missing_data = False
        if fname == "":
            logger.info("fname field missing")
            fname_invalid = True
            missing_data = True

        if uname == "":
            logger.info("uname field missing")
            uname_invalid = True
            missing_data = True

        if passwd == "":
            logger.info("passwd field missing")
            passwd_invalid = True
            missing_data = True

        if missing_data:
            flash("Missing information", "error")
            retVal = render_template(
                template_name, fname=fname, fname_invalid=fname_invalid,
                uname=uname, uname_invalid=uname_invalid, passwd=passwd,
                passwd_invalid=passwd_invalid)
        else:
            logger.debug("Send Person create to server")
            try:
                p = utils.get_protocol()
                p.new_actor(uname, passwd)
                logger.info("User %s created", uname)
                flash("User creation complete")
                retVal = redirect(url_for(".index"))
            except protocol.ProtocolException as e:
                logger.exception("Hathi instance encountered an error creating user")
                flash("Hathi instance encountered an error creating user")
                retVal = render_template(template_name)
            except Exception as e:
                logger.exception("Exception encountered creating user")
                for a in e.args:
                    flash(a, "error")
                retVal = render_template(template_name)
    else:
        raise NotImplementedError("Method %s" % (request.method))

    return retVal


def _collect_note(obj):
    logger = logging.getLogger("Front")

    link = url_for("note.index", id=obj["@id"])
    summary = note.collect_note(obj)[0]

    if summary is None or summary == "":
        logger.debug("Using ID for summary")
        summary = obj["@id"]

    return (link, summary)


def _collect_person(obj):
    link = url_for("actor.index", id=obj.id)
    name = actor.get_name(obj)
    return (link, name)


def _dig_object(url, from_create=False):
    logger = logging.getLogger("Front")

    logger.debug("Dig through %s", url)
    obj = utils.getObject(url)
    retVal = None
    t = obj.types
    if "Create" in t:
        if isinstance(obj["object"], str):
            logger.debug("Follow object created")
            retVal = _dig_object(obj["object"])
        else:
            obj = obj["object"]
            if not isinstance(obj, core.ASObj):
                raise TypeError("Embedded object in \"%s\" not an AS object")
            else:
                logger.debug("Embedded object of correct type")
                t = obj.types
                if "Note" in t:
                    logger.debug("Embedded note")
                    retVal = _collect_note(obj)
                elif "Person" in t:
                    logger.debug("Ignoring actor Create object")
                else:
                    retVal = "%s type handling unimplemented" % (t)
    elif "Note" in t:
        logger.debug("Note ID")
        retVal = _collect_note(obj)
    elif "Person" in t:
        if not from_create:
            logger.debug("Person ID")
            retVal = _collect_person(obj)
        else:
            logger.debug("Ignoring actor Create object")
    else:
        retVal = "%s type handling unimplemented" % (t)

    return retVal


def _process_collection(url):
    logger = logging.getLogger("Front")

    logger.debug("Processing %s as collection", url)
    obj = core.ASObj(utils.getObject(url))

    if not obj["@type"] in ["Collection", "OrderedCollection"]:
        raise TypeError("%s is not a collection-based object" % (url))

    retVal = []
    try:
        items = obj["items"]
        logger.debug("Value of items: %s", items)
        if isinstance(items, str):
            logger.debug("Collection value is a string")
            retVal.append(_dig_object(items))
        elif isinstance(items, list):
            logger.debug("Collection value is a list")
            for i in items:
                try:
                    logger.debug("Processing item %s", i)
                    val = _dig_object(i)
                    if val is not None:
                        if isinstance(val, str):
                            logger.debug("Value to add: %s", val)
                        else:
                            logger.debug("Value to add: (%s, %s)",
                                         val[0], val[1])
                        retVal.append(val)
                except Exception:
                    logger.exception(
                        "Exception encountered processing \"%s\"", i
                    )
                    raise
        else:
            raise TypeError(
                "%s didn't convert to a Python string or list" % (url)
            )
    except KeyError as e:
        logger.info("%s member not found in JSON", e)

    return retVal


def _get_collection(collection):
    logger = logging.getLogger("Front")
    retVal = None
    actor_id = current_user.get_id()
    logger.debug("Value of collection: %s", collection)
    template = "collectionview.html"
    page_name = collection.title()

    try:
        logger.info("Get actor \"%s\" \"%s\" collection",
                    actor_id, collection)
        data = utils.getObject(actor_id)
        if not data:
            flash("Actor \"%s\" doensn't exist" % (actor_id), "warning")
            retVal = render_template(template, collection=None,
                                     page_name=page_name)
        else:
            actor_obj = core.ASObj(data)
            logger.debug("Process collection")
            coll_list = _process_collection(actor_obj[collection])
            retVal = render_template(template, collection=coll_list,
                                     page_name=page_name)
    except Exception:
        flash("Error encountered getting actor", "error")
        logger.exception("Error encountered getting actor")
        retVal = make_response(
            render_template(template, collection=None,
                            page_name=page_name), 500)

    return retVal


@bp.route("/inbox", methods=["GET"])
@login_required
def inbox():
    return _get_collection("inbox")


@bp.route("/outbox", methods=["GET"])
@login_required
def outbox():
    return _get_collection("outbox")


@bp.route("/following", methods=["GET"])
@login_required
def following():
    return _get_collection("following")


@bp.route("/followers", methods=["GET"])
@login_required
def followers():
    return _get_collection("followers")
# vim: set expandtab:ts=4:sw=4
