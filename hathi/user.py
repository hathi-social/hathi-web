# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""flask_login.UserMixin subclass"""

import logging

from flask_login import UserMixin

from . import utils, protocol

class User(UserMixin):
    """User session object"""

    def __init__(self):
        super().__init__()
        self._logger = logging.getLogger("User")
        self._session = None
        self._protocol = utils.get_protocol()

        self.person = None

    def get_id(self):
        return self._session

    def get_user(self, session, actor):
        """Used by flask_login.user_loader callback to get the user information
        object."""

        try:
            if self._protocol.check_session(session, actor):
                self._logger.debug("Session active")
                self._session = session
                return self

            self._logger.debug("Session not valid")
        except protocol.ProtocolException:
            self._logger.error("Protocol exception encountered while checking session")

        return None

    def login(self, uname, passwd):
        """Login user"""

        rslt = self._protocol.login(uname, passwd)
        self._logger.debug("Value of rslt: %s", rslt)
        if rslt:
            self._logger.debug("Credential verified")
            self._session = rslt
            return True

        self._logger.debug("Credential verification failed")
        return False


# vim: set expandtab:ts=4:sw=4
